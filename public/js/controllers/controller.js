/**
  *
  * This is the Angular controller for the `account.html` view. It provides the
  * underlying data and functionality for the profile/my account section of
  * the site. Users can view and update this information.
  * 
  * @description Angular controller for `account.html` view    
  *
  */
appCtrl.controller('accountController', ['$rootScope', '$state', '$scope', '$window', 'account', function($rootScope, $state, $scope, $window, account) {

  $scope.data = {};
  $scope.data.display_name  = '';
  $scope.data.user_title    = '';
  $scope.data.user_location = '';
  
  if($rootScope.authed === true) {
    account.fetch().then(function(data) {
      $scope.user = data;
    })

    $scope.update = function() {
      $scope.data.display_name  = $scope.user.user_display_name;
      $scope.data.user_title    = $scope.user.user_title;
      $scope.data.user_location = $scope.user.user_location;
      $scope.data.token         = $window.sessionStorage.token;
      account.update($scope.data).then(function(data) {
        $scope.data.display_name  = '';
        $scope.data.user_title    = '';
        $scope.data.user_location = '';
        $state.go('account', {}, { reload: true });
      })
    };
    
  } else {
    $state.go('login', {}, { reload: true })
  }

}]);

/**
  *
  * This is the Angular controller for the `board.html` view. It provides
  * the underlying data and functionality for the main board listing on
  * the site. It renders the `threads` for users to interact with.
  * 
  * @description Angular controller for `board.html` view
  *
  */
appCtrl.controller('boardController', ['$rootScope', '$state', '$scope', '$window', 'board', function($rootScope, $state, $scope, $window, board) {
  
  board.fetch().then(function(data) {
    $scope.thread = data
  })
  board.reply_count().then(function(data) {
    $scope.replyCount = data.replies
  })
    
}]);

/**
  *
  * This is the Angular controller for the `newthread.html` view. Bad naming.
  * It provides the underlying functionality for the `new thread` feature
  * on the site. It enables users to post new threads.
  * 
  * @description Angular controller for `newthread.html` view
  *
  */
appCtrl.controller('postController', ['$rootScope', '$state', '$scope', '$window', 'board', function($rootScope, $state, $scope, $window, board) {

  $scope.data = {}
  $scope.data.thread_title = ''
  $scope.data.thread_body = ''
  $scope.data.thread_author = ''
  
  if($rootScope.authed === true) {
      $scope.submit = function() {
        
        $scope.data.thread_title = this.thread_title
        $scope.data.thread_body = this.thread_body
        $scope.data.thread_author = $rootScope.user.id
        
        board.new_thread($scope.data).then(function(data) {
          $state.go('board', {}, { reload: true })
        })

      }
  } else {
    $state.go('login', {}, { reload: true })
  }

  $scope.data.thread_title = ''
  $scope.data.thread_body = ''
  $scope.data.thread_author = ''
  
}]);

/**
  *
  * This is the Angular controller for the `thread.html` view. It provides 
  * the underlying data and functionality for an individual `thread`
  * on the site. It enables users to see a `thread` comments and add
  * new comments to the `thread`.
  * 
  * @description Angular controller for `thread.html` view
  *
  */
appCtrl.controller('threadViewController', ['$rootScope', '$state', '$scope', '$window', 'board', function($rootScope, $state, $scope, $window, board) {

  $scope.thread = {}
  $scope.reply = {}
  $rootScope.thread

    board.fetch_thread().then(function(data) {
      $scope.thread = data.thread
      $rootScope.thread = data.thread
    })
  
  $scope.submit = function() {
    
    $scope.reply.reply_body = this.reply_body
    $scope.reply.thread_id = $rootScope.thread.id
    
    board.new_reply($scope.reply).then(function(data) {
      $state.go("thread", { slug: $rootScope.thread.slug }, { reload: true })
    })
  }
  
}]);

/**
  *
  * This is the Angular controller for the `profile.html` view. It provides 
  * the underlying data and functionality for an individual `profile`
  * on the site. It enables users to see another user's public profile.
  * 
  * @description Angular controller for `profile.html` view
  *
  */
appCtrl.controller('profileController', ['$state', '$scope', '$window', 'profile', function($state, $scope, $window, profile) {
  
  
  
}]);

/**
  *
  * This is the Angular controller for the `home.html` view. It does not
  * currently serve any purpose, but is a placeholder for future
  * functionality for the board home page. This may include:
  * different topical boards, highlights, etc.
  * 
  * @description Placeholder Angular controller for now
  *
  */
appCtrl.controller('mainController', ['$state', '$scope', '$window', 'main', function($state, $scope, $window, main) {
  
  // Maybe eventually show stuff on home page, but this is otherwise unnecessary for now
  
}]);

/**
  *
  * This is the Angular controller for the `signup.html` view. It provides 
  * the underlying functionality for an individual to `signup` on the 
  * board. 
  * 
  * @description Angular controller for `signup.html` view
  *
  */
appCtrl.controller('signupController', ['$state', '$scope', '$window', 'user', function($state, $scope, $window, user) {

  $scope.data = {}
  $scope.data.first_name = ''
  $scope.data.last_name = ''
  $scope.data.email = ''
  $scope.data.username = ''
  $scope.data.password = ''

  $scope.register = function() {
    //do some form of form logic and submit thing
    $scope.data.first_name = this.first_name
    $scope.data.last_name = this.last_name
    $scope.data.email = this.email
    $scope.data.username = this.username
    $scope.data.password = this.password
    user.register($scope.data).then(function(data){
      $scope.username = ''
      $scope.password = ''
      $scope.first_name = ''
      $scope.last_name = ''
      $scope.email = ''
      $state.go('login', {}, { reload: true })
    })
  }
  
}]);

/**
  *
  * This is the Angular controller for the `login.html` view. It provides 
  * the underlying data and functionality for an individual `login`
  * on the site. 
  * 
  * @description Angular controller for `login.html` view
  *
  */
appCtrl.controller('loginController', ['$rootScope', '$state', '$scope', '$window', 'user', function($rootScope, $state, $scope, $window, user) {

  $scope.data = {}
  $scope.data.username = ''
  $scope.data.password = ''
  
  if($rootScope.authed === false) {
    $scope.login = function() {
      //do some form of form logic and submit thing
      $scope.data.username = this.username
      $scope.data.password = this.password
      user.login($scope.data).then(function(loginData) {
        $scope.data.username = '';
        $scope.data.password = '';
        $rootScope.authed = true;
        $state.go('account', {}, { reload: true });
      }).catch(function(status, data) {
        /* eslint-disable no-console */
        console.log(status)
        console.log(data)
        /* eslint-disable no-console */
      })
    }
  } else {
    $state.go('login', {}, { reload: true })
  }
  
}]);

/**
  *
  * This is the Angular controller for the site navigation. It provides 
  * the underlying functionality to navigate across the various pages
  * and handles session auth and control for login-restricted areas.
  * 
  * @description Angular controller for site navigation
  *
  */
appCtrl.controller('navController', ['$rootScope', '$state', '$scope', '$window', 'account', function($rootScope, $state, $scope, $window, account) {

  $scope.logout = function() {
    account.query().then(function(data) {
      if(data.success === true) {
        account.logout()
        $rootScope.authed = false
        $state.go('login', {}, { reload: true })
      }
    })
  };
  
}]);

/**
  *
  * This is the Angular controller for the `members.html` view. It provides 
  * the underlying data and functionality for a listing of all board members
  * that users can comb through to view public profiles.
  * 
  * @description Angular controller for `members.html` view
  *
  */
appCtrl.controller('membersController', ['$rootScope', '$state', '$scope', '$window', 'member', function($rootScope, $state, $scope, $window, member) {

  if($rootScope.authed === true) {
    member.fetch().then(function(data) {
      $scope.list = data.users;
    })
  } else {
    $state.go('login', {}, { reload: true })
  }
  
  
}]);
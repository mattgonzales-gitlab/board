/**
  *
  * This is the Angular service for authentication. It checks the
  * `/api/query` route to determine if a user is currently
  * authenticated and returns the session key as `response.data`
  * 
  * @description Angular service for confirming user authentication
  *
  */
appSrv.service('auth', ['$http', function($http) {
  return {
    query : function() {
      return $http.get('/api/query').then(function(response) {
        return response.data
      })
    }
  }
}])

/**
  *
  * This is the Angular service for the primary board functionality. 
  * It manages all features from listing the board contents to
  * creating new threads or replies. This is the core service for
  * the board.
  * 
  * @description Angular service for board functionality
  *
  */
appSrv.service('board', ['$http', '$stateParams', function($http, $stateParams) {
  return {
    fetch : function() {
      return $http.get('/api/board/fetch').then(function(response) {
        return response.data
      })
    },
    fetch_thread : function() {
      return $http.get('/api/board/fetch_thread',  { params: { slug: $stateParams.slug }}).then(function(response) {
        return response.data
      })
    },
    new_thread : function(threadData) {
      return $http.post('/api/board/new_thread', threadData)
    },
    new_reply : function(replyData) {
      return $http.post('/api/board/new_reply', replyData)
    },
    reply_count : function() {
      return $http.get('/api/board/thread_reply_count', { params: { $stateParams }}).then(function(response) {
        return response.data
      })
    }
  }
}])

/**
  *
  * This is the Angular service for any user-related functionality.
  * It handles register, login, and listing for now. More features
  * to come later.
  * 
  * @description Angular service for user functionality
  *
  */
appSrv.service('user', ['$http', function($http) {
  return {
    register : function(registerData) {
      return $http.post('/api/user/register', registerData);
    },
    login : function(loginData) {
      return $http.post('/api/user/login', loginData);
    },
    list : function() {
      return $http.get('/api/user/list').then(function(response) {
        return response.data
      })
    }
  }
}])

/**
  *
  * This is the Angular service for any account-related functionality.
  * It processes data for displaying in the profile view and
  * enables users to edit their account information.
  * 
  * @description Angular service for account functionality
  *
  */
appSrv.service('account', ['$http', function($http) {
  return {
    update : function(accountData) {
      return $http.post('/api/account/update', accountData)
    },
    fetch : function() {
      return $http.get('/api/account/fetch').then(function(response) {
        return response.data
      })
    },
    query : function() {
      return $http.get('/api/query').then(function(response) {
        return response.data
      })
    },
    logout : function() {
      return $http.get('/api/account/logout').then(function(response) {
        return response.data
      })
    }
  }
}])

/**
  *
  * This is the Angular service for any member functionality.
  * It primarily handles fetching member data. Could probably
  * be merged with `user` or `account`.
  * 
  * @description Angular service for member functionality
  *
  */
appSrv.service('member', ['$http', function($http) {
  return {
    fetch : function() {
      return $http.get('/api/member/fetch').then(function(response) {
        return response.data
      })
    }
  }
}])

/**
  *
  * This is the Angular service for any profile functionality.
  * It handles fetching profile data and could probably be
  * merged into `user` or `account`.
  * 
  * @description Angular service for profile functionality
  *
  */
appSrv.service('profile', ['$http', function($http) {
  return {
    fetch : function() {
      return $http.get('/api/profile/fetch').then(function(response) {
        return response.data
      })
    }
  }
}])
/* @description Loads various functions into the root `app` module for Angular. */
var app = angular.module('app', ['ui.router', 'angular-jwt', 'appCtrl', 'appSrv']);

/* @description Creates an `appCtrl` angular module for the view controller functions */
var appCtrl = angular.module('appCtrl', []);

/* @description Creates an `appSrv` angular module for the services functions */
var appSrv = angular.module('appSrv', []);

/* @description Configures the app and injects various providers for the app's functionality */
app.config(['$stateProvider', '$urlRouterProvider', '$urlMatcherFactoryProvider', '$locationProvider', '$httpProvider',
  function($stateProvider, $urlRouterProvider, $urlMatcherFactoryProvider, $locationProvider, $httpProvider) {

  $locationProvider.html5Mode(true).hashPrefix('!')
  
  $stateProvider.state('home', {
    url: '/',
    templateUrl: '/views/home.html',
    controller: 'mainController',
    access: { authenticate: false }
  });
  
  $stateProvider.state('members', {
    url: '/members',
    templateUrl: '/views/members.html',
    controller: 'membersController',
    access: { authenticate: true }
  });
  
  $stateProvider.state('signup', {
    url: '/signup',
    templateUrl: '/views/signup.html',
    controller: 'signupController',
    access: { authenticate: false }
  });
  
  $stateProvider.state('login', {
    url: '/login',
    templateUrl: '/views/login.html',
    controller: 'loginController',
    access: { authenticate: false }
  });
  
  $stateProvider.state('board', {
    url: '/board',
    templateUrl: '/views/board.html',
    controller: 'boardController',
    access: { authenticate: true }
  });
  
  $stateProvider.state('admin', {
    url: '/admin',
    templateUrl: '/views/admin.html',
    controller: 'adminController',
    access: { authenticate: true }
  });
  
  $stateProvider.state('account', {
    url: '/account',
    templateUrl: '/views/account.html',
    controller: 'accountController',
    access: { authenticate: true }
  });
    
  $stateProvider.state('newthread', {
    url: '/new',
    templateUrl: '/views/newthread.html',
    controller: 'postController',
    access: { authenticate: true }
  });
    
  $stateProvider.state('thread', {
    url: '/thread/:slug',
    templateUrl: '/views/thread.html',
    controller: 'threadViewController',
    access: { authenticate: true }
  });
    
  $stateProvider.state('profile', {
    url: '/profile/:slug',
    templateUrl: '/views/profile.html',
    controller: 'profileController',
    access: { authenticate: true }
  });     
    
  $stateProvider.state('404', {
    templateUrl: '/views/404.html',
    access: { authenticate: false },
    data: {
      title: 'Page Not Found'
    }
  });
  
  $urlRouterProvider.otherwise(function($injector, $location) {
    $injector.get('$state').go('404');
    return $location.path();
  });
  
}]);

/* @description Runs the Angular app and handles global authentication and scoping */
app.run(function($rootScope, $location, auth) {
    auth.query().then(function (user) {
      console.log("user, post-auth: ", user)
      console.log("authed? ", user.success)
            if (user.success === true) {
              $rootScope.authed = true;
              $rootScope.user = user.user
              console.log("authed? ", user.success)
              console.log("user? ", $rootScope.user)
              console.log("user subset: ", $rootScope.user.email)
            } else {
              $rootScope.authed = false;
            }
        });
});
// --------------
// Imports
// --------------
var express       = require('express'),
    sequelize     = require('sequelize'),
    dbconn        = require('./dbconn'),
    path          = require('path'),
    bcrypt        = require('bcrypt'),
    router        = express.Router();
      
module.exports = router;

/**
  *
  * Check session authentication. Used for flagging views and,
  * eventually, other permissions based on authentication.
  *
  * @param {Object} req.session.key - contains the `user` object for authentication
  * @description Authentication query for user logins/sessions
  *
  */
  router.get('/api/query', function(req, res) {
    console.log(req);
    if(req.session.key) {
      return res.send({ success: true, msg: 'Authenticated', user: req.session.key })
    } else {
      return res.send({ success: false, msg: 'Not Authenticated' })
    }
  })
  
  /**
    *
    * Find all user accounts from users table via sequelize. Fail the request if the
    * user is not authenticated. This prevents non-logged in users from
    * viewing the user list.
    *
    * @param {Array} users - Contains JSON-ified output from sequelize `findAll` query of
    *       all users in the `users` table.
    * @param {Object} req.session.key - contains the `user` object for authentication
    * @description Fetches the list of members for the board to display on roster page
    *
    */
  router.get('/api/member/fetch', function(req, res) {
    if(req.session.key) {
      dbconn.users.findAll( { order: 'id DESC' }).then(function(users) {
      return res.send({ success: true, users: users }); 
      
      });
    } else {
      return res.send({ success: false, msg: 'Not Authenticated - Member' })
    }
    
  });

  /**
    *
    * Queries the database to pull all of the created `threads`. Compounds the query with inclusion
    * of the `users` and `replies` tables to associate them for front-end rendering. 
    * 
    * @param {Object[]} threads - an array of `threads` objects from the findAll query, which includes
    *       relationships of `users` and `replies`.
    * @description Fetches the board thread data to create the board index    
    *
    */
  router.get('/api/board/fetch', function(req, res) {
    dbconn.threads.findAll({ 
      order: sequelize.literal('threads.id DESC'),
      raw: true,
      attributes: {
        include: ['title', 'body', 'slug', 'createdAt', 'userId', [sequelize.fn('COUNT', 'replies.id'), 'replyCount']]
      }, 
      include: [{
        model: dbconn.users,
      }, {
        model: dbconn.replies, attributes: [],
      }],
      group: ['threads.id', 'replies.threadId'],
    }).then(function(threads) {
      return res.send({ success: true, threads: threads }); // return all threads found in table

    });
    
  });

  /**
    *
    * When a user clicks on a thread, this route queries the database to find
    * that thread and the related users and replies to render the entire
    * thread contents on the frontend.
    * 
    * @param {Object[]} thread - Finds the `thread` a user clicks on and combines the related
    *     `users` and `replies` associated with `thread`.
    * @description Fetches the contents of an individual thread to display the thread view
    *
    */
  router.get('/api/board/fetch_thread', function(req, res) {
    dbconn.threads.findOne({ 
      where: { slug: req.query.slug },
      include: [{
        model: dbconn.users,
        attributes: ['id', 'username', 'first_name']
      }, {
        model: dbconn.replies,
        attributes: ['id', 'body', 'author', 'createdAt', 'updatedAt'] // for last edited & posted date, reply author, reply content, etc.
      }]
    }).then(function(thread) {
      return res.send({ success: true, thread: thread }); // return all threads found in table

    });
    
  });
     
  /**
    *
    * Queries the database for the user's own account information to render in the browser on the
    * `profile` page. The request fails if the user does not exist and redirects to `login`.
    * 
    * @param {Object[]} user - the user object of the authenticated user
    * @param {Object} req.session.key - contains the `user` object for authentication
    * @description Fetches the data for an individual account to display on the profile page(s)    
    *
    */
  router.get('/api/account/fetch', function(req, res) {
    if(req.session.key) {
      dbconn.users.findOne({
        where: { id: req.session.key.id }
      }).then(function(user) {
        if (!user) {
          return res.send({ success: false, msg: 'Could not pull user information. ' })
        } else {
          return res.status(200).send(user);
        }
      })
    } else {
      res.redirect('/login')
    }
    
  });

  /**
    *
    * Destroys the user session when a user logs out.
    *
    * @param {Object} req.session.key - contains the `user` object for authentication
    * @description Destroys the authenticated user session to log out    
    *
    */
 router.get('/api/account/logout', function(req, res) {
   if(req.session.key) {
     req.session.destroy()
     return res.send({ success: true, msg: 'Log out successful' })
   } else {
     return res.send({ success: false, msg: 'An error occured' })
   }
 })

  /**
    *
    * Submits `newUser` to backend for entry into `users` table. Returns a
    * success or fail message based on outcome.
    *
    * @param {Object} user - the newly registered `user` data
    * @description Handles registration data ingestion and account creation 
    *
    */  
  router.post('/api/user/register', function(req, res) {
    dbconn.users.create({
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      email: req.body.email,
      username: req.body.username,
      password: req.body.password,
      is_admin: '0'
    }).then(function(user) {
      return res.json({ success: true, msg: 'Successfully created new user,' })
    }).error(function(err) {
      return res.json({ success: false, msg: err })
    })
  })
  
  /**
    *
    * Processes the `login` form data to authenticate a user into the board. Creates
    * a session key with the `user` object for validation in other areas.
    *
    * @param {Object} user - user data from SQL query
    * @description Handles the login authentication and processing functionality    
    *
    */  
  router.post('/api/user/login', function(req, res) {
    dbconn.users.findOne({
      where: { username: req.body.username }
    }).then(function(user) {
      if (!user)
        res.send({ success: false, msg: 'Authentication failed. Account not found.' });
      if (!bcrypt.compareSync(req.body.password, user.password)) {
        // var token = jwt.sign(user, config.secret.key)
        res.send({ success: false, msg: 'Authentication failed. Wrong password.' });
      } else {
        req.session.key = user
        res.send({ success: true, msg: 'Authentication successful.' });
      }
    });
  })
  
  /**
    *
    * When a user updates their profile, this route updates the table entry with
    * the new data provided.
    *
    * @param {Object} req.session.key - contains the `user` object for authentication
    * @param {Object} user - user data from SQL query
    * @description Updates an account detail based on what fields are updated in the profile
    *
    */  
  router.post('/api/account/update', function(req, res) {
    if(req.session.key) {
      dbconn.users.findOne({
        where: { id: req.session.key.id }
      }).then(function(user) {
        if (!user) {
          res.send({ success: false, msg: 'User does not exist.' })
        } else {
          user.updateAttributes({
            user_display_name: req.body.display_name,
            user_title: req.body.user_title,
            user_location: req.body.user_location
          }).then(function() {
            return res.status(200).send(user)
          })
        }
      })
    } else {
      $state.go('login')
    }
  })
  
  /**
    *
    * When a user updates their profile, this route updates the table entry with
    * the new data provided.
    *
    * @param {Object} req.session.key - contains the `user` object for authentication
    * @param {Object} thread - thread data from SQL query
    * @description Processes the data to create a new thread on the board    
    *
    */
  router.post('/api/board/new_thread', function(req, res) {
    if(req.session.key) {
      var slug = req.body.thread_title.replace(/\ /g, '-');
      dbconn.threads.create({
      title: req.body.thread_title,
      body: req.body.thread_body,
      slug: slug,
      userId: req.session.key.id
    }).then(function(thread) {
      return res.send({ success: true, thread: thread });
    }).error(function(err) {
      return res.send({ success: false, msg: "Could not create user." });
    });
    } else {
      $state.go('login')
    }
  })
  
  /**
    *
    * When a user updates their profile, this route updates the table entry with
    * the new data provided.
    *
    * @param {Object} req.session.key - contains the `user` object for authentication
    * @param {Object} thread - thread data from SQL query
    * @description Processes the data to create a new reply within an individual thread    
    *
    */
  router.post('/api/board/new_reply', function(req, res) {
    if(req.session.key) {
      dbconn.replies.create({
      body: req.body.reply_body,
      author: req.session.key.username,
      userId: req.session.key.id,
      threadId: req.body.thread_id
    }).then(function(thread) {
      return res.send({ success: true, thread: thread });
    }).error(function(err) {
      return res.send({ success: false, msg: "Could not create user." });
    });
    } else {
      $state.go('login')
    }
  })
  
  router.get('*', function(req, res) {
    res.sendFile('index.html', { root: path.join(__dirname, '../public/views/') });
  });

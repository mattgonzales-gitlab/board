var Sequelize = require('sequelize'),
    bcrypt    = require('bcrypt'),
    conf      = require('../config/sql');

/* Establish connection to MySQL db */
var sql  = new Sequelize(conf.db.name, conf.db.username, conf.db.password, { 
  host: conf.db.host,
  dialect: 'mysql',

  pool: {
    max: 5,
    min: 0,
    idle: 10000
  },
  
});

/* Create models for sequelize */
var models = {};

/*
 *
 * @description Defines the sequelize model for the `threads` table
 * to store information about individual board threads. 
 *
 */
models.threads = sql.define('threads', {
  title: {
    type: Sequelize.STRING,
    allowNull: false
  },
  body: {
    type: Sequelize.TEXT('long'),
    allowNull: false
  },
  slug: {
    type: Sequelize.STRING,
    allowNull: false
  }
}, {
  freezeTableName: true // Model tableName will be the same as the model name
});

/*
 *
 * @description Defines the sequelize model for the `replies` table
 * to store information about individual replies associatd with `threads`. 
 *
 */
models.replies = sql.define('replies', {
  body: {
    type: Sequelize.STRING,
    allowNull: false
  },
  author: {
    type: Sequelize.STRING,
    allowNull: false
  },
  reported: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: 0
  }
}, {
    
  freezeTableName: true
});

/*
 *
 * @description Defines the sequelize model for the `reports` table
 * to store information about individual `threads` or `replies that
 * have been reported or flagged for moderation. 
 *
 */
models.reports = sql.define('reports', {
  reporter: {
    type: Sequelize.STRING,
    allowNull: false
  },
  body: {
    type: Sequelize.STRING,
    allowNull: false
  }
});

/*
 *
 * @description Defines the sequelize model for the `users` table
 * to store information about individual accounts created on the board. 
 *
 */
models.users = sql.define('users', {
    first_name: {
      type: Sequelize.STRING,
      allowNull: false
    },
    last_name: {
      type: Sequelize.STRING,
      allowNull: false
    },
    email: {
      type: Sequelize.STRING,
      allowNull: false
    },
    password: { /* Password hashing for db storage  */
      type: Sequelize.STRING,
      set: function (val) {
        var salt = bcrypt.genSaltSync(10),
            hash = bcrypt.hashSync(val, salt);

        this.setDataValue('password', hash);
      },
      allowNull: false
    },
    username: {
      type: Sequelize.STRING,
      allowNull: false
    },
    user_display_name: {
      type: Sequelize.STRING
    },
    user_title: {
      type: Sequelize.STRING
    },
    user_location: {
      type: Sequelize.STRING
    },
    profile_slug: {
      type: Sequelize.STRING
    },
    is_admin: {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: 0
    }
}, {
  hooks: { /* Check password to avoid re-hashing each time */
    beforeCreate: function(data, options, callback) {
      data.step = 1;
      if (data.code === null) {
        data.code = urlsafe64.encode(crypto.randomBytes(15));
      }
      return callback();
    }
  }
});


/* Establish relationships between tables */

/*
 *
 * @description `threads` have many `replies` and `replies`
 * belong to only one `threads`
 *
 */
models.replies.belongsTo(models.threads);
models.threads.hasMany(models.replies);

/*
 *
 * @description `users` have many `replies` and `replies`
 * belong to only one `users`
 *
 */
models.replies.belongsTo(models.users);
models.users.hasMany(models.replies);

/*
 *
 * @description `users` have many `threads` and `threads`
 * belong to only one `users`
 *
 */
models.threads.belongsTo(models.users);
models.users.hasMany(models.threads);

/*
 *
 * @description `users` and `replies` have many `reports` and `reports`
 * belong to only one `replies` and only one `users`.
 *
 */
models.reports.belongsTo(models.users);
models.reports.belongsTo(models.replies);
models.users.hasMany(models.reports);
models.replies.hasMany(models.reports);

// Synchronizes the database on load and makes necessary changes
// if updates are made to the models or structure.
sql.sync().done(function(){ console.log('Updated DB') });

module.exports = models;
// Imports
var express        = require('express'),
    routes         = require('./app/routes'),
    morgan         = require('morgan'),
    sequelize      = require('sequelize'),
    session        = require('express-session'),
    bodyParser     = require('body-parser'),
    methodOverride = require('method-override'),
    dbconn         = require('./config/sql'),
    exports        = module.exports = {},
    app            = express();

/*
 *
 * @description Middleware to parse data to JSON and enables access of 
 * req.body for various requests
 *
 */
app.use(bodyParser.urlencoded({extended: false }));
app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));
app.use(morgan('dev'))

/*
 * 
 * @description Sets the rules and guidelines for the router
 * in the app as a control mechanism for data flow.
 *
 */
app.all('*', function(req, res, next) {
  res.set('Access-Control-Allow-Origin', 'http://localhost:8080');
  res.set('Access-Control-Allow-Credentials', true);
  res.set('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT');
  res.set('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Authorization');
  if ('OPTIONS' == req.method) return res.send(200);
  next();
});

/*
 *
 * @description Ensures support for clients that only support simple
 * GET and POST verbs
 *
 */
app.use(methodOverride('X-HTTP-Method-Override')); 

/*
 *
 * @description sets the port for accessing the served website and
 * sets the root directory for the views (html files) to render
 *
 */
app.set('port', process.env.PORT || 8080);
app.set('views', __dirname + '/public/views');

// Use a static directory to serve up static content
app.use('/', express.static(__dirname + '/public'));

// Condensed routes for simplicity
app.use('/', routes);
 
// Default syntax to listen on port 8081 for requests
var server = app.listen(app.get('port'), function() {
  console.log('Listening on port: 8080');
});

// expose app           
exports.app = app;